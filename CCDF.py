#!/usr/bin/env python
# coding: utf-8

#CCDF example
from collections import Counter
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

sns.set(style="whitegrid") #set style of plot

#read a file with the following format (separated by ;) with the header rt,category !
# rt must be in seconds, like:

# rt;category
# 0.050913258;B
# 0.0064644285;C
# 0.019394973;D
# 0.023638617900000002;A
# 0.5949751999999999;B
# 0.14754300050000002;C
# 0.4551129375;D
data = pd.read_csv('sample/response_times.txt', sep=';')

#create figure without showing it
#size is in inches for some reason
fig, ax = plt.subplots(ncols=1, nrows=1, figsize=(10, 4), dpi=300);

def create_ccdf_data(dataframe):
    rt = dataframe.rt.to_list() #get rt from dataframe
    #convert response times to ms and then round
    #we count the occurrences of each of these times
    ctr = Counter([int(t*1000) for t in rt])
    tuples = [(e, ctr[e], ctr[e]/len(rt)) for e in ctr] # group in tuple like: (rt, occurrences of rt, freq of rt)
    tuples = sorted(tuples, key=lambda x: x[0]) #sort by rt

    #probably can be done easier with numpy and vector ops but I think this is clearer to understand the process
    #add cumulative freq
    tuples_acc = []
    acc = 0
    for tup in tuples:
        rt, occ, freq = tup #unpack group
        tuples_acc.append((rt, occ, freq, 1-acc, acc))
        acc+=freq # <== key step here

    df = pd.DataFrame(tuples_acc, columns=['rt', 'occ', 'freq', '1-acc', 'acc'])
    return df

#for each category we create the parsed dataframe with the ccdf
for category in data.category.unique():
    #filter data by current category
    cat_data = data[data.category == category]
    #get dataframe with ccdf data
    cat_df = create_ccdf_data(cat_data)
    #plot line for this category
    sns.lineplot(data=cat_df, x='rt', y='1-acc', ax=ax, label=category)

#change global chart stuff
ax.set_ylabel('P(T > x)') #prob of response time being higher than x
ax.set_xlabel('Response Time (ms)')
ax.set_xscale("log") #set scale to log
ax.set_title('Complementary cumulative distribution function')

plt.tight_layout() #https://matplotlib.org/stable/tutorials/intermediate/tight_layout_guide.html

fig.savefig('ccdf.pdf')

fig.savefig('sample/ccdf.pdf')
fig.savefig('ccdf.png')