# Complementary cumulative distribution function (CCDF) 

![CCDF chart](ccdf.png)

Use:
```
virtualenv env -p `which python3`
source env/bin/activate
pip install pandas seaborn matplotlib
python CCDF.py
```
